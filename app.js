var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');



var app = express();

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/mean-angular5', { useNewUrlParser: true, promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));
  require("./models/user.model");
  require("./models/checklist.model");
  require("./models/etiquette.model");
  require("./models/piecejointe.model");
  require("./models/comment.model");
  require("./models/securite.model");
  require("./models/carte.model");
  require("./models/liste.model");
  require("./models/tableau.model");
  require("./models/equipe.model");


var book = require('./routes/api/book.route');
var todo = require('./routes/api/todo.route');
var user = require('./routes/api/user.route');
var equipe = require('./routes/api/equipe.route');
var tableau = require('./routes/api/tableau.route');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist/mean-angular5')));
app.use('/login', express.static(path.join(__dirname, 'dist/mean-angular5')));
app.use('/book', book);
app.use('/todos', todo);
app.use('/user', user);
app.use('/equipe', equipe);
app.use('/tableau', tableau);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;