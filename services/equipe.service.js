var Equipe = require("../models/equipe.model");
var Securite = require("../models/securite.model");
var mongo = require('mongodb');

_this = this;

exports.getEquipes = async function (req) {
    try{
        return await Equipe.find(req);
    }
    catch(e){
        throw Error ("Error while find All Equipe");
    }
}


exports.getSecurites = async (req)=>{
    try{
        return await Securite.find(req);
    }
    catch(e){
        throw Error ("Error while find All Securite");
    }
}

exports.createEquipeSimple = async (req)=>{
    var newEquipe = new Equipe({
        val : req.body.val,
        desce : req.body.desce,
        owner : req.body.owner
    });
    try{
        var save = await newEquipe.save();
        return save;
    }
    catch(e){
        throw Error ("Error while Creating Equipe");
    }
}

exports.createSecurite = async (req)=>{
    var news = new Securite({
        val : req.body.val,
        desce : req.body.desce
    });
    try{
        var save = await news.save();
        return save;
    }
    catch(e){
        throw Error ("Error while Creating Equipe");
    }
}

exports.findById = async function(id){
    try{
        return await Equipe.findById(id);
    }
    catch(e){
        throw Error ("Error while Creating Equipe");
    }
}

exports.createEquipeComplexe = async (req)=>{
    var id = req.body._id;
    if(id != undefined && id != null){
        this.findById(id).then(
            equipe => {
                console.log(equipe);
                equipe.membres = req.body.membres;
                equipe.tableaux = req.body.tableaux;
                equipe.owner = req.body.owner;
                try{
                    var save = equipe.save();
                    return save;
                }
                catch(e){
                    throw Error ("Error while Creating Equipe : " + e.message);
                }
            }
        );
    }
    else{
        var equipe = new Equipe({
            val : req.body.val,
            desce : req.body.desce,
            membres : req.body.membres,
            tableaux : req.body.tableaux,
            owner : req.body.owner
        });
        try{
            var save = equipe.save();
            return save;
        }
        catch(e){
            throw Error ("Error while Creating Equipe : " + e.message);
        }
    }
}

exports.getEquipeByOwner = async (req) => {
    try{
        var o_id = req.params.idOwner;
        var equipe = Equipe.find({"owner._id" : req.params.idOwner});
        return equipe;
    }
    catch(e){
        throw Error ("Error while Creating Equipe : " + e.message);
    }
}

exports.updateEquipeTableau = async (req) =>{
    try{
        var equipe = req.body.equipe;
        this.findById(equipe._id).then(
            equipe =>{
                
            }
        )
        return equipe;
    }
    catch(e){
        throw Error ("Error while Updating Equipe : " + e.message);
    }
}