var User = require("../models/user.model");

_this = this;

exports.getUsers = async function (req){
    try{
        return await User.find(req);
    }
    catch(e){
        throw Error ("Error while find All User");
    }
}

exports.getUserAuthenticate = async function(req){
    try{
        var mailAddress = req.body.mailAddress;
        var password = req.body.password;
        var query = {"mailAddress" : mailAddress, "password" : password};
        return await User.find(query);
    }
    catch (e){
        throw Error("Your credential is wrong");
    }
}

exports.createUser = async function (req){
    var newUser = new User({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        password : req.body.password,
        mailAddress : req.body.mailAddress,
        address : req.body.address
    });
    try{
        var save = await newUser.save();
    }
    catch(e){
        throw Error ("Error while Creating User");
    }
}

exports.findById = async function(id){
    try{

    }
    catch(e){
        throw Error ("Error get User by Id");
    }
}