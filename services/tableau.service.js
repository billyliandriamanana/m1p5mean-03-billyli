var Tableau = require("../models/tableau.model");
var mongo = require('mongodb');

_this = this;

exports.getTableaux = async function (req) {
    try{
        return await Tableau.find(req);
    }
    catch(e){
        throw Error ("Error while find All Tableau");
    }
}


exports.findById = async function(id){
    try{
        return await Tableau.findById(id);
    }
    catch(e){
        throw Error ("Error while Creating Tableau");
    }
}

exports.createTableau = async function(req){
    try{
        var tableau = new Tableau({
            val : req.body.val,
            desce : req.body.desce,
            equipeId : req.body.equipeId,
            securite : req.body.securite
        })

        return tableau.save();
    }
    catch(e){
        throw Error ("Error while Creating Tableau : " + e.message);
    }

}



