import { Component, OnInit } from '@angular/core';
import { EquipeService } from "../services/equipe.service";
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import Equipe from "../models/equipe.model";
import Securite from '../models/securite.model';
import Tableau from '../models/tableau.model';
declare var $: any;

@Component({
  selector: 'app-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.css']
})
export class TableauComponent implements OnInit {

  constructor(private equipeService : EquipeService, private router : Router, private route : ActivatedRoute, private app : AppComponent) { }

  userConnected = this.app.userConnected;
  equipes : Equipe[] = [];
  color : string[] = ["success","info","danger","warning"];
  equipeCreate = {"owner" : this.app.userConnected};
  tableauCreate : any  = {"securite" : {}};
  tableauObject : Tableau ;
  securites : Securite[] = [];

  ngOnInit() {
    this.app.titleContent = "Tableaux";
    this.getEquipeByOwner();
    this.getSecurite();
    
  }

  getEquipeByOwner(){
    this.equipeService.getEquipeByOwner(this.userConnected._id).subscribe(
      equipes => {
        console.log(equipes);
        this.equipes = equipes;
        this.getColors();
      }
    );
  }

  getColors(){
    var colorTemp = ["success","info","danger","warning"];
    var i = 0;
    this.equipes.forEach((value,index)=> {
      this.color.push(colorTemp[i]);
      i++;
      if(i >= 4)
        i = 0;
    });
  }

  getColor(index){
    return this.color[index];
  }

  createEquipe(){
    console.log(this.equipeCreate);
    this.equipeService.createEquipeSimple(this.equipeCreate).subscribe(
      equipe =>{
        this.equipes.push(equipe);
        $('#exampleModal').modal('toggle');
      }
    )
  }

  showmodal(i){
    $("#tableau-" + i).modal();
  }

  getSecurite(){
    this.equipeService.getSecurite().subscribe(
      securite => {
        this.securites = securite;
      }
    );
  }

  createTableau(){
    
    this.updateSecuriteOnTableau();

    console.log(this.tableauCreate);

  }

  updateEquipeOnTableau(){
    this.equipes.map( res => {
        if(res._id == this.tableauCreate.equipe._id){
          this.tableauCreate.equipe.val = res.val;
          this.tableauCreate.equipe.desce = res.desce;
          this.tableauCreate.equipe.membres = res.membres;
        }
    });

    
  }

  updateSecuriteOnTableau(){
    this.securites.map(res => {
        if(res._id == this.tableauCreate.securite._id){
          this.tableauCreate.securite.val = res.val;
          this.tableauCreate.securite.desce = res.desce;
        }
    });
  }


}
