import Securite from "./securite.model";
import Liste from "./liste.model";

class Tableau{
    _id : string;
    val : string;
    desce : string;
    created_date: Date;
    securite : Securite;
    listes : Liste[];
    equipeId : string;

    constructor(){
        this.val = "";
        this.desce = "";
        this.created_date = new Date();
        this.securite = new Securite();
        this.listes = [] ;
        this.equipeId = "";
        
    }
};

export default Tableau;