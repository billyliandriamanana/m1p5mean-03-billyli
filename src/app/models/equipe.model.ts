import User from "./user.model";
import Tableau from "./tableau.model";

class Equipe{
    _id : string;
   val : string;
   desce : string;
   membres : User [];
   tableaux : Tableau [];
   created_date: Date;
   owner : User;

   constructor(){
    this.val = "";
    this.desce = "";
    this.created_date = new Date();
    this.membres = [];
    this.tableaux = [];
    this.owner = new User();
}
};

export default Equipe;