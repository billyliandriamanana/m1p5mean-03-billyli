import Membre from "./user.model";
import Etiquette from "./etiquette.model";
import CheckList from "./checklist.model";
import PieceJointe from "./piecejointe.model";
import Comment from "./comment.model";

class Carte{
    _id : string;
    val : string;
    desce : string;
    created_date: Date;
    limite_date_start : Date;
    limite_date_end : Date;
    membres : Membre[];
    etiquettes : Etiquette[];
    checkLists : CheckList[];
    pieceJointes : PieceJointe[];
    comments : Comment[];

    constructor(){
        this.val = "";
        this.desce = "";
        this.created_date = new Date();
        this.membres = [];
        this.etiquettes = [];
        this.pieceJointes = [];
        this.comments = [];
    }

};

export default Carte;