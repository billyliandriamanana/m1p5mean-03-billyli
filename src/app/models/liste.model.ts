import Carte from "./carte.model";
class Liste{
    _id : string;
    val : string;
    desce : string;
    created_date: Date;
    cartes : Carte[];


    constructor(){
        this.val = "";
        this.desce = "";
        this.created_date = new Date();
        this.cartes = [];
    }
};

export default Liste;