import User from "./user.model";

class Comment{
    _id : string;
    val : string;
    desce : string;
    created_date: Date;
    updated_date: Date;
    membre : User;

    constructor(){
        this.val = "";
        this.desce = "";
        this.created_date = new Date();
        this.membre = new User();
    }
};

export default Comment;