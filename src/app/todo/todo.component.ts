import { Component, OnInit } from '@angular/core';
import ToDo from '../models/todo.model';
import { TodoService } from '../services/todo.service';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(private todoService: TodoService) { 
    
  }


   //Declaring the new todo Object and initilizing it
   public newTodo: ToDo = new ToDo()

   //An Empty list for the visible todo list
   todosList: ToDo[];
   editTodos: ToDo[] = [];

  ngOnInit() {
    //At component initialization the 
    this.todoService.getToDos()
      .subscribe(todos => {
        console.log("todoList", todos);
        //assign the todolist property to the proper http response
        this.todosList = todos
        console.log(todos)
      });
  }

  //This method will get called on Create button event
  
  create() {
    console.log("new todo ",this.newTodo);
    this.todoService.createTodo(this.newTodo)
      .subscribe((res) => {
        this.todosList.push(res.data)
        this.newTodo = new ToDo()
      })
  }


  editTodo(todo: ToDo) {
    console.log(todo)
    if(this.todosList.includes(todo)){
      if(!this.editTodos.includes(todo)){
        this.editTodos.push(todo)
      }else{
        this.editTodos.splice(this.editTodos.indexOf(todo), 1)
        this.todoService.editTodo(todo).subscribe(res => {
          console.log('Update Succesful')
        }, err => {
          this.editTodo(todo)
          console.error('Update Unsuccesful')
        })
      }
    }
  }

  doneTodo(todo:ToDo){
    todo.status = 'Done'
    this.todoService.editTodo(todo).subscribe(res => {
      console.log('Update Succesful')
    }, err => {
      this.editTodo(todo)
      console.error('Update Unsuccesful')
    })
  }

  submitTodo(event, todo:ToDo){
    if(event.keyCode ==13){
      this.editTodo(todo)
    }
  }



}
