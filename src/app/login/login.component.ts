import { Component, OnInit } from '@angular/core';
import User from "../models/user.model";
import { UserService } from "../services/user.service";
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService : UserService, private router : Router, private route : ActivatedRoute, private app : AppComponent) { }

  userConnected : User;
  userForm = {} ;
  errorMessage;

  ngOnInit() {
    
  }

  authenticate(){
    this.app.isLoading = true;
      this.userService.userAuthenticate(this.userForm).subscribe(
        res => {
          if( res != null && res.length > 0 ){
            this.app.isLoggedIn = true;
            this.app.userConnected =  res[0];
            this.router.navigate(["tableau"]);
            this.app.isLoading = false;
          }
          else{
            if(this.app.isDebug){
              this.app.isLoggedIn = true;
              this.app.userConnected =  new User();
              this.router.navigate(["tableau"]);
              this.app.isLoading = false;
            }
            console.log("tsy tafa e");
            this.errorMessage = "Your cedential is wrong. Please try again!";
          }
        }, err => {
          this.errorMessage = "Your cedential is wrong. Please try again!";
        }
      )
  }
}
