import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../../app.component';
import User from '../../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute, private app : AppComponent) { }

  user : User;
  title = "Trello.mg";
  _app = this.app;

  ngOnInit() {
    this.user = this.app.userConnected;
  }

}
