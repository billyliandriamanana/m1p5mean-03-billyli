import User from "../models/user.model";
import { Observable } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response, Http} from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    userUrl = "/user";
    constructor(
        private http: HttpClient
    ){}

    userAuthenticate(user) : Observable<User[]>{

        return this.http.post(`${this.userUrl}/authenticate`, user).
        map( res =>{
            return res["data"] as User[];
        });
    }
}