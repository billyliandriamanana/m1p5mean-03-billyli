import Equipe from "../models/equipe.model";

import { Observable } from 'rxjs';
import { map, filter, scan } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response, Http} from '@angular/http';
import { Injectable } from '@angular/core';
import Securite from "../models/tableau.model";

@Injectable()
export class EquipeService{
    url = "/equipe";
    constructor(
        private http: HttpClient
    ){}

    getEquipeByOwner(idOwner) : Observable<Equipe[]>{
        return this.http.get(`${this.url}/owner/${idOwner}`).
            map(res => {
                return res["data"] as Equipe[];
            });
    }

    createEquipeSimple(equipe) : Observable<Equipe>{

        return this.http.post(`${this.url}/simple`,equipe).
            map( res => {
                return res["data"] as Equipe;
            })

    }

    getSecurite() : Observable<Securite[]>{
        return this.http.get(`${this.url}/securite`).
            map(res => {
                return res["data"] as Securite[];
            })
    }
}