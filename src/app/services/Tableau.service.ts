import Tableau from "../models/tableau.model";

import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TableauService{
    url = "/tableau";
    constructor(
        private http: HttpClient
    ){}

    getTableaux() : Observable<Tableau[]>{
        return this.http.get(`${this.url}/`).
            map(res => {
                return res["data"] as Tableau[];
            });
    }

    createTableau(tableau) : Observable<Tableau>{

        return this.http.post(`${this.url}`,tableau).
            map( res => {
                return res["data"] as Tableau;
            })

    }

}