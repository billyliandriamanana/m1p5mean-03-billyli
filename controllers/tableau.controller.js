var TableauService = require("../services/tableau.service");

_this = this;

exports.getTableaux = async (req,res,next)=>{
    try{
        
        var tableau = await TableauService.getTableaux({});
        return res.status(200).json({status: 200, data: tableau, message : "Get All Tableaux"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.createTableau = async (req,res,next) => {
    try{
        
        var tableau = await TableauService.createTableau(req);
        return res.status(200).json({status: 200, data: tableau, message : "Tableau created"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}