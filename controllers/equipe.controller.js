var EquipeService = require("../services/equipe.service");

_this = this;

exports.getEquipes = async (req,res,next)=>{
    try{
        
        var equipe = await EquipeService.getEquipes({});
        return res.status(200).json({status: 200, data: equipe, message : "Get All Equipes"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.createEquipeSimple = async (req,res,next) => {
    try{
        
        var equipe = await EquipeService.createEquipeSimple(req);
        return res.status(200).json({status: 200, data: equipe, message : "Equipe created"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}


exports.createSecurite = async (req,res,next) => {
    try{
        
        var equipe = await EquipeService.createSecurite(req);
        return res.status(200).json({status: 200, data: equipe, message : "Securite created"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}


exports.createEquipeComplexe = async (req,res,next) => {
    try{
        
        var equipe = await EquipeService.createEquipeComplexe(req);
        return res.status(200).json({status: 200, data: equipe, message : "Equipe create succesfully"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.getEquipeByOwner = async (req,res,next) => {
    try{
        var equipe = await EquipeService.getEquipeByOwner(req);
        return res.status(200).json({status: 200, data: equipe, message : "Get Equipe by owner succesfully"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.getSecurite = async (req,res,next)=>{
    try{
        
        var securite = await EquipeService.getSecurites({});
        return res.status(200).json({status: 200, data: securite, message : "Get All Equipes"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

