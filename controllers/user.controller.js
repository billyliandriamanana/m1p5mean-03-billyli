var UserService = require("../services/user.service");

_this = this;

exports.getUsers = async (req,res,next)=>{
    try{
        
        var userConnected = await UserService.getUsers({});
        return res.status(200).json({status: 200, data: userConnected, message : "Get All User"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.getUserAuthenticate = async (req,res,next)=>{
    try{
        
        var userConnected = await UserService.getUserAuthenticate(req);
        return res.status(200).json({status: 200, data: userConnected, message : "User is succesfully connected"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : e.message});
    }
}

exports.createUser = async (req,res,next)=>{
    try{
        
        var user = await UserService.createUser(req);
        return res.status(200).json({status: 200, data: user, message : "User is succesfully created"});
    }
    catch(e){
        return res.status(400).json({status: 400, message : "User not created : " + e.message});
    }
}