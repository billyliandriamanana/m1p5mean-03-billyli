var express = require('express')

var router = express.Router()
var todos = require('./api/todo.route');
var books = require('./api/book.route');


router.use('/todos', todos);
router.use('/books', books);


module.exports = router;