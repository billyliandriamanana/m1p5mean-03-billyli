var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var TableauController = require('../../controllers/tableau.controller');

router.get('/',TableauController.getTableaux);

router.post('/',TableauController.createTableau);

module.exports = router;