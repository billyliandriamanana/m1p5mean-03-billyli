var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var UserController = require('../../controllers/user.controller');

router.get('/',UserController.getUsers);

/* GET User Authenticate */
router.post('/authenticate',UserController.getUserAuthenticate);

router.post('/',UserController.createUser);

module.exports = router;