var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var EquipeController = require('../../controllers/equipe.controller');

router.get('/',EquipeController.getEquipes);

router.get('/securite',EquipeController.getSecurite);

router.post('/securite',EquipeController.createSecurite);

router.post('/simple',EquipeController.createEquipeSimple);

router.post('/complexe',EquipeController.createEquipeComplexe);

router.get("/owner/:idOwner",EquipeController.getEquipeByOwner);

module.exports = router;