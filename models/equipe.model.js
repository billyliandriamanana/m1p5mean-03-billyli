var  User = require('mongoose').model("User").schema;
var  Tableau = require('mongoose').model("Tableau").schema;

var mongoose = require('mongoose');
var Equipechema = new mongoose.Schema({ 
   val : String,
   desce : String,
   membres : [User] ,
   tableaux : [Tableau],
   created_date: { type: Date, default: Date.now },
   owner : User
});

module.exports = mongoose.model("Equipe", Equipechema);