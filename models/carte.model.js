var  Membre = require('mongoose').model("User").schema;
var  Etiquette = require('mongoose').model("Etiquette").schema;
var  CheckList = require('mongoose').model("CheckList").schema;
var  PieceJointe = require('mongoose').model("PieceJointe").schema;
var  Comment = require('mongoose').model("Comment").schema;


var mongoose = require('mongoose');
var Cartechema = new mongoose.Schema({
    val : String,
    desce : String,
    created_date: { type: Date, default: Date.now },
    limite_date_start : Date,
    limite_date_end : Date,
    membres : [Membre],
    etiquettes : [Etiquette],
    checkLists : [CheckList],
    pieceJointes : [PieceJointe],
    comments : [Comment]

});

module.exports = mongoose.model('Carte', Cartechema);