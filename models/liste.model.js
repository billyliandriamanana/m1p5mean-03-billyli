var  Carte = require('mongoose').model("Carte").schema;
var mongoose = require('mongoose');
var ListeSchema = new mongoose.Schema({
    val : String,
    desce : String,
    created_date: { type: Date, default: Date.now },
    cartes : [Carte]
});

module.exports = mongoose.model('Liste', ListeSchema);