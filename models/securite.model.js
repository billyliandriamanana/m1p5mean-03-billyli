var mongoose = require('mongoose');
var SecuriteSchema = new mongoose.Schema({
   val : String,
   desce : String,
   created_date: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Securite", SecuriteSchema);