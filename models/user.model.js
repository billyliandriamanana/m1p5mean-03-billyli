var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema({
    firstName : String,
    lastName : String,
    mailAddress : String,
    password : String,
    address : String
});

const User = mongoose.model("User", UserSchema);

module.exports = User;