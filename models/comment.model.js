var  User = require('mongoose').model("User").schema;

var mongoose = require('mongoose');
var CommentSchema = new mongoose.Schema({
    val : String,
    desce : String,
    created_date: { type: Date, default: Date.now },
    updated_date: Date,
    membre : User
});

module.exports = mongoose.model('Comment', CommentSchema);