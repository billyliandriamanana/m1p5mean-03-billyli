var  Securite = require('mongoose').model("Securite").schema;
var  Liste = require('mongoose').model("Liste").schema;
var mongoose = require('mongoose');

var ObjectID = require('mongodb').ObjectID;

var mongoose = require('mongoose');
var TableauSchema = new mongoose.Schema({
    val : String,
    desce : String,
    created_date: { type: Date, default: Date.now },
    securite : Securite,
    listes : [Liste],
    equipeId : mongoose.Schema.Types.ObjectId

});

module.exports = mongoose.model('Tableau', TableauSchema);